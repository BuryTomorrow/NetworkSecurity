package com.burytomorrow.weixinhongbao;

import android.accessibilityservice.AccessibilityService;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Toast;

import java.util.List;

import static android.view.accessibility.AccessibilityEvent.*;
import static android.view.accessibility.AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED;
import static android.view.accessibility.AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED;
import static android.view.accessibility.AccessibilityNodeInfo.ACTION_CLICK;

/**
 * Created by BuryTomorrow on 2018/5/22.
 */

public class MyService extends AccessibilityService {
    private static final int MSG_BACK = 233;

    private static final String UI_RECEIVE = "com.tencent.mm.plugin.luckymoney.ui.LuckyMoneyReceiveUI";
    private static final String UI_DETAIL = "com.tencent.mm.plugin.luckymoney.ui.LuckyMoneyDetailUI";

    private boolean luckyClicked, hasLucky;

    private final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == MSG_BACK) {
                performGlobalAction(GLOBAL_ACTION_BACK);
                postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        performGlobalAction(GLOBAL_ACTION_BACK);
                    }
                }, 1000);
            }
        }
    };

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        //接收事件，如果触发了通知栏变化、界面变化则执行
        final int eventType = event.getEventType();
        //通知栏事件
        if (eventType == TYPE_NOTIFICATION_STATE_CHANGED) {
            luckyClicked = false;
            //获取通知栏信息内容
            List<CharSequence> texts = event.getText();
            //检查是否有红包
            if(!texts.isEmpty()){
                for(CharSequence t:texts){
                    String text = String.valueOf(t);
                    if(text.contains("[微信红包]")){
                        //打开微信
                        luckyClicked = true;
                        if(event.getParcelableData()==null || !(event.getParcelableData() instanceof Notification)) return;
                        //打开微信的通知栏消息
                        //获取Notification对象
                        Notification notification = (Notification) event.getParcelableData();
                        //调用其中的PendingIntent，打开微信界面
                        PendingIntent pendingIntent = notification.contentIntent;
                        try{
                            pendingIntent.send();
                        }catch (PendingIntent.CanceledException e){
                            e.printStackTrace();
                        }
                        break;
                    }
                }
            }
        }

        //如果当前界面属于微信界面
        else{
//            Toast.makeText(this,event.getClassName().toString(),Toast.LENGTH_LONG).show();
            //分两种情况，1.红包界面；2.当前界面是聊天列表界面或聊天内容界面
            //1,红包有未拆完红包的情况和拆完红包的情况
            //未拆：
            //System.out.println("sasaaadadasdasdasdasdasdas");
            if("com.tencent.mm.plugin.luckymoney.ui.LuckyMoneyReceiveUI"
                    .equals(event.getClassName().toString())){

                // 获取当前界面的祖先节点
                AccessibilityNodeInfo nodeInfo = getRootInActiveWindow();
                traverseNode(nodeInfo);
            }
            //拆完：
            else if ( "com.tencent.mm.plugin.luckymoney.ui.LuckyMoneyDetailUI"
                    .equals(event.getClassName().toString()) ){
                // 延时100ms后，调用handler线程执行全局后退的操作，使界面返回到聊天界面
                handler.sendEmptyMessageDelayed(MSG_BACK, 100);
            }
            //剩下的就是在聊天列表界面或聊天内容界面
            else{
                // 获取当前界面的祖先节点
                AccessibilityNodeInfo nodeInfo = getRootInActiveWindow();
                if(nodeInfo==null) return;

                // 微信列表界面，显示聊天信息用的控件为View，利用ViewId获取控件,得到8个聊天列表项
                List<AccessibilityNodeInfo> ListNodes =
                        nodeInfo.findAccessibilityNodeInfosByViewId("com.tencent.mm:id/apx");
                if (null != ListNodes && ListNodes.size() != 0){
                    for(int i=ListNodes.size()-1 ; i>=0; i--){
                        // 对这8个列表内容的文本匹配“[微信红包]”字样
                        if ( !ListNodes.get(i).getText().toString().contains("[微信红包]") ) continue;
                        // 如果存在红包，则找到其父节点（能点击的控件LinearLayout）模拟点击，进入聊天界面
                        AccessibilityNodeInfo node = ListNodes.get(i).getParent();
                        if ( node!=null && node.isClickable() ){
                            node.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                            break;
                        }
                    }
                }
                //已经在聊天内容界面当中，则需要找到"领取红包"字样，然后从父节点找到可点击的打开红包按钮
                List<AccessibilityNodeInfo> ChatNodes =
                        nodeInfo.findAccessibilityNodeInfosByText("领取红包");

                if (null != ChatNodes && ChatNodes.size() != 0){
                    // 如果存在，则找到其父节点（能点击的控件LinearLayout）模拟点击
                    for(int i=ChatNodes.size()-1 ; i>=0; i--){
                        AccessibilityNodeInfo node = ChatNodes.get(i).getParent();
                        int t = 8;
                        while(t>0 &&node!=null){
                            t--;
                            if ( node!=null && node.isClickable() ){
                                node.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                break;
                            }
                            node = node.getParent();
                        }

                    }
                }
            }
        }
    }

    private void traverseNode(AccessibilityNodeInfo node) {
        if (null == node) return;
        final int count = node.getChildCount();
        if (count > 0) {
            //Toast.makeText(this,String.valueOf(count),Toast.LENGTH_SHORT).show();
            for (int i = 0; i < count; i++) {
                AccessibilityNodeInfo childNode = node.getChild(i);
               // Toast.makeText(this,childNode.getClassName().toString(),Toast.LENGTH_SHORT).show();
                if (null != childNode && childNode.getClassName().equals("android.widget.Button") && childNode.isClickable()) {
                    childNode.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                    hasLucky = true;
                }
                traverseNode(childNode);
            }
        }
    }
    @Override
    protected void onServiceConnected(){
        super.onServiceConnected();
        luckyClicked = true;
        Toast.makeText(this,"已开启自动抢红包服务",Toast.LENGTH_SHORT).show();
        //授权成功后执行
    }
    @Override
    public void onInterrupt() {
        //关闭服务
        Toast.makeText(this,"已关闭自动抢红包服务",Toast.LENGTH_SHORT).show();
    }

}
