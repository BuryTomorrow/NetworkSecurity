package com.burytomorrow.weixinhongbao;

import android.Manifest;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.widget.Button;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Button AccessibleLabelButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkPermission();//申请许可

        AccessibleLabelButton = (Button) findViewById(R.id.button);

        Listener();//设置监听事件
    }
    //start后执行onResume,pause后同样也是执行onResume
    @Override
    protected void onResume() {
        super.onResume();
        //更改按钮的显示
        boolean isAccessibilityEnabled = isAccessibleEnabled();
        AccessibleLabelButton.setBackgroundResource(isAccessibilityEnabled ? R.drawable.circle_green : R.drawable.circle);
        AccessibleLabelButton.setText(isAccessibleEnabled() ? "已开启" : "未开启");
    }

    //判断AccessibleLabel服务允许打开
    private boolean isAccessibleEnabled() {
        AccessibilityManager manager = (AccessibilityManager) getSystemService(Context.ACCESSIBILITY_SERVICE);
        List<AccessibilityServiceInfo> runningServices = manager.getEnabledAccessibilityServiceList(AccessibilityEvent.TYPES_ALL_MASK);
        for (AccessibilityServiceInfo info : runningServices) {
            if (info.getId().equals(getPackageName() + "/.MyService")) {
                return true;
            }
        }
        return false;
    }

    private void Listener(){
        //辅助功能开关事件
        AccessibleLabelButton.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent sSettingsIntent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
                startActivity(sSettingsIntent);
            }
        });
    }

    private void checkPermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) return;
        String[] permissions = new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE};

        boolean flag = true;
        for (String s : permissions) {
            if (checkSelfPermission(s) != PackageManager.PERMISSION_GRANTED) {
                flag = false;
                break;
            }
        }

        if (!flag) requestPermissions(permissions, 233);
    }
}
