package com.burytomorrow.weixinxposed;

import android.app.Application;
import android.app.assist.AssistStructure;
import android.content.ContentValues;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.lang.reflect.Method;
import java.util.HashMap;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

/**
 * Created by BuryTomorrow on 2018/5/27.
 */

public class HookToast implements IXposedHookLoadPackage{
    @Override
    public void handleLoadPackage(XC_LoadPackage.LoadPackageParam loadPackageParam) throws Throwable {
        final HashMap<String,Long> redBagIdMap = new HashMap<String,Long>();
        //收到消息时会触发insert方法，参考http://www.shareditor.com/blogshow?blogId=138
        XposedHelpers.findAndHookMethod("com.tencent.wcdb.database.SQLiteDatabase",loadPackageParam.classLoader,
                "insert",String.class,String.class, ContentValues.class,new XC_MethodHook(){
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable{
                Object tableName = param.args[0];
                Object obj1 = param.args[1];
                if("message".equals(tableName) && obj1.equals("msgId")){
                    ContentValues cv = (ContentValues)param.args[2];
                    String content = cv.getAsString("content");
                    Log.i("周三晚group_6:","content: " +content);
                    //获取当前红包消息的时间戳值，存到全局的Map中,然后解析xml消息中的paymsgid值
                    long creatTime = cv.getAsLong("createTime");
                    Log.i("周三晚group_6:","createTime: " +creatTime);
                    if(!TextUtils.isEmpty(content)){
                        //找到ID在XML中的位置
                        int startID = content.indexOf("<paymsgid>");
                        String payMsgId="";
                        if(startID!=-1){
                            //截取子字符串，也就获得相应的ID
                            payMsgId = content.substring(startID+19,startID+19+31);
                        }
                        if(!TextUtils.isEmpty(payMsgId)){
                            //将ID和创建时间放入Map当中，也就保存了红包信息的唯一ID值以及其创建时间
                            //从第13位开始截取，获取后面18位，因为第13位ID值会因为用户不同而不同
                            redBagIdMap.put(payMsgId.substring(13),creatTime);
                        }
                    }
                }
            }

        });


        XposedHelpers.findAndHookMethod("com.tencent.mm.plugin.luckymoney.ui.i", loadPackageParam.classLoader,
                "getView", int.class, View.class, ViewGroup.class, new XC_MethodHook() {
                    @Override
                    protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                        int index = (Integer) param.args[0];
                        Object obj = param.thisObject;
                        //找到sJ方法
                        Method method = obj.getClass().getDeclaredMethod("sJ",int.class);
                        method.setAccessible(true);
                        Object itemObj = method.invoke(obj,index);
                        Object owJ = itemObj.getClass().getField("owJ").get(itemObj);//红包的ID值
                        Object oxe = itemObj.getClass().getField("oxe").get(itemObj);//金额
                        Object oxf = itemObj.getClass().getField("oxf").get(itemObj);//时间戳
                        Object oxr = itemObj.getClass().getField("oxr").get(itemObj);//微信昵称
                        Object oxs = itemObj.getClass().getField("oxs").get(itemObj);//
                        Object oxt = itemObj.getClass().getField("oxt").get(itemObj);//留言
                        Object oxu = itemObj.getClass().getField("oxu").get(itemObj);//手气最佳信息
                        Object userName = itemObj.getClass().getField("userName").get(itemObj);//微信用户ID名

                        //根据红包的ID值从Map当中取得红包的创建时间
                        String payMsgId = (String)owJ;
                        //注意oxf获得的时间戳只有10位长度,而在数据库中获取的创建时间是13位
                        long time = Long.parseLong((String)oxf)*1000;
                        long createTime = redBagIdMap.get(payMsgId.substring(13));
                        long cost = time-createTime;
                        //Log.i("周三晚group_6:"," costTime : "+cost);
                        //根据抢红包的时间来对相应的TextView输出信息
                        Object result = param.getResult();
                        if(result instanceof LinearLayout){
                            LinearLayout itemView = (LinearLayout) result;
                            //获取原本微信红包详情信息界面中展示时间戳信息的TextView控件
                            LinearLayout leftLayout = (LinearLayout)itemView.getChildAt(1);
                            TextView timeText = (TextView)leftLayout.getChildAt(3);
                            timeText.setTextColor(0xFFFF0000);
                            double costTime = ((double)cost)/1000;
                            Log.i("周三晚group_6:"," costTime : "+costTime);
                            if(costTime<=2){
                                timeText.setText("花了 "+costTime+"s,这么快，肯定是挂逼！");
                            }else if(costTime<=4){
                                timeText.setText("花了 "+costTime+"s,还行，手速一般般！");
                            }else{
                                timeText.setText("花了 "+costTime+"s,慢死了，渣渣！");
                            }
                        }
                    }
                });

        XposedHelpers.findAndHookMethod(Application.class, "attach", Context.class, new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable{
                //这种方式解决了多dex应用hook找不到类的方法
                ClassLoader cl = ((Context)param.args[0]).getClassLoader();
                Class<?> hookclass = null;
                try {
                    hookclass = cl.loadClass("com.tencent.mm.plugin.luckymoney.ui.i");
                }catch (Exception e){
                    return;
                }
                XposedHelpers.findAndHookMethod(hookclass, "sJ", int.class, new XC_MethodHook() {
                    @Override
                    protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                        //参数信息
//                            public String owJ;//当前红包ID值
//                            public long oxe;//金额
//                            public String oxf;//时间戳
//                            public String oxr;//微信昵称
//                            public String oxs;
//                            public String oxt;
//                            public String oxu;
//                            public String userName;//微信ID
                        Object obj = param.getResult();
                        Object owJ = obj.getClass().getField("owJ").get(obj);//红包的ID值
                        Object oxe = obj.getClass().getField("oxe").get(obj);//金额
                        Object oxf = obj.getClass().getField("oxf").get(obj);//时间戳
                        Object oxr = obj.getClass().getField("oxr").get(obj);//微信昵称
                        Object oxs = obj.getClass().getField("oxs").get(obj);//
                        Object oxt = obj.getClass().getField("oxt").get(obj);//留言
                        Object oxu = obj.getClass().getField("oxu").get(obj);//手气最佳信息
                        Object userName = obj.getClass().getField("userName").get(obj);//微信用户ID名

                        Log.i("周三晚group_6:","owJ : "+owJ+"  ,oxe : "+oxe+"  ,oxf : "+oxf+"  ,oxr : "+oxr
                        +"  ,oxs : "+oxs+"   ,oxt : "+oxt+"  ,oxu : "+oxu+"  ,userName : "+userName);

                    }
                });
            }
        });
    }
}
